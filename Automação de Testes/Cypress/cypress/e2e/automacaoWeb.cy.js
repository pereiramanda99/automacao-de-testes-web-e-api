describe('Correios', () => {
  it('busca correios - cep', () => {
    //acessando o site dos correios
    cy.visit('http://www.buscacep.correios.com.br/sistemas/buscacep/')
    //inserindo cep no campo de busca
    cy.get('#endereco').type('69005-040')
    //realizar busca
    cy.get('#btn_pesquisar').click()
  })

  it('busca correios - endereço', () => {
    //acessando o site dos correios
    cy.visit('http://www.buscacep.correios.com.br/sistemas/buscacep/')
    //inserindo cep no campo de busca
    cy.get('#endereco').type('Lojas Bemol')
    //realizar busca
    cy.get('#btn_pesquisar').click()
  })
})

describe('Booking', () => {
  it('verificação', () => {
    //acessando o site booking
    cy.visit('http://www.booking.com/')
    //inserindo manaus no campo de busca
    cy.get('#ss').type('Manaus')
    //selecionando data
    cy.get(':nth-child(2) > .xp__dates-inner > .xp__group > .sb-date-field > .sb-searchbox__input > .sb-date-field__icon > .bk-icon > path').click()
    //data de check-in
    cy.get('[data-date="2022-08-13"]').click()
    //data de check-out
    cy.get('[data-date="2022-08-16"]').click()
    //realizando busca
    cy.get('.sb-searchbox__button').click()
    //ordenando
    cy.get('[data-testid="sorters-dropdown-trigger"] > .e57ffa4eb5').click()
    //selecionando mais avaliadas
    cy.get(':nth-child(9) > .fc63351294 > .a1b3f50dcd > .b1e6dd8416 > span').click()
    //verificando nome do primeiro da lista
    cy.get('[data-testid="title"]').contains("Hotel Villa Amazônia")
    //verificando avaliação
    cy.get('[data-testid="review-score"]').contains("9.2")
    //verificando valor
    cy.get('[data-testid="price-and-discounted-price"]').contains("US$722")
  })
})